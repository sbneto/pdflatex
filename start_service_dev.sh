#!/usr/bin/env bash

docker build -t registry.gitlab.com/sbneto/pdflatex .
docker run --rm -p "8000:8000" registry.gitlab.com/sbneto/pdflatex
