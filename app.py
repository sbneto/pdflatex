import logging
import os
import subprocess
import tempfile
from zipfile import ZipFile
from io import BytesIO
from xmlrpc.server import SimpleXMLRPCServer
from socketserver import ThreadingMixIn

logger = logging.getLogger(__name__)


def run_subprocess(command, input_data, **kwargs):
    process = subprocess.run(
        command,
        input=input_data,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        **kwargs
    )
    result = {
        'returncode': process.returncode,
        'stdout': process.stdout,
        'stderr': process.stderr,
    }
    return result


class RPCFunctions:
    def pdflatex(self, file_data, main_file='main.tex'):
        logger.info('Received file, rendering pdf...')
        with tempfile.TemporaryDirectory() as folder:
            ZipFile(BytesIO(file_data)).extractall(folder)
            command = ['pdflatex', '-halt-on-error', main_file]
            result = run_subprocess(command, file_data, cwd=folder)
            pdf_file_name, _, _ = main_file.rpartition('.')
            pdf_file_name += '.pdf'
            if result['returncode'] == 0:
                try:
                    result['data'] = open(os.path.join(folder, pdf_file_name), 'rb').read()
                except FileNotFoundError:
                    pass
            return result


class SimpleThreadedXMLRPCServer(ThreadingMixIn, SimpleXMLRPCServer):
    pass


if __name__ == '__main__':
    with SimpleThreadedXMLRPCServer(("0.0.0.0", 8000), use_builtin_types=True) as server:
        logger.info("Listening on port 8000...")
        server.register_instance(RPCFunctions())
        server.serve_forever()
