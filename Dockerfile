FROM sbneto/phusion-python:3.6

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN apt-get update \
    && apt-get install -y texlive-full texlive-lang-portuguese \
    && pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir --upgrade -r requirements.txt \
	&& apt-get autoremove -yqq \
	&& apt-get clean \
	&& rm -Rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

# INSTALLING SERVICE
COPY etc /etc
RUN chmod 750 /etc/service/pdflatex/run

COPY app.py /app/app.py

EXPOSE 8000
CMD ["/sbin/my_init"]
