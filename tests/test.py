from os import path
from xmlrpc.client import ServerProxy
from tika import parser


BASE_FOLDER = path.dirname(path.abspath(__file__))
proxy = ServerProxy('http://127.0.0.1:8000/', use_builtin_types=True)


def test_pdf_rendering():
    with open(path.join(BASE_FOLDER, 'test_pdf_rendering.zip'), 'rb') as f:
        pdf_data = proxy.pdflatex(f.read())
    file = parser.from_buffer(pdf_data['data'])
    assert pdf_data['returncode'] == 0 and file['content'] == """







































CARTA CONVITE

PARA COMPOSIÇÃO DE MEMBRO EM BANCA EXAMINADORA

Prezada DAINF - Profa. Doutora Maici Duarte Leite,

Temos a imensa satisfação de convidá-la para participar como membro da Banca Exa-

minadora do Seminário de Trabalho de Conclusão de Curso ( 1 ) intitulado NOVAS TEC-

NOLOGIAS PARA A EDUCAÇÃO que será apresentado no dia 6 de novembro de

2017 (Segunda-feira) às 19h30min na sala Q204.

Aguardo sua confirmação com a simples resposta “ACEITE” por este e-mail e antecipa-

damente agradeço a sua participação.

Francisco Beltrão-PR, 13 de abril de 2019.

Andreza Quintas - 123456
(Discente)

DAINF - Prof. Doutor Eng. Francisco Antonio Fernandes Reinaldo
(Orientador)


"""
